# -*- coding: utf-8 -*-
"""
Author: Marcus MacDonell.
Project: Acoustic Vector Network Analyser
    
    This script runs runs a set of 5 calibration measurements using an Agilent 3395A for a TRRM calibration (16 Term).
    The GPIB data is from each measurement is captured, tabulated and plotted/printed to CLI for inspection. 
    The calibration is performed utilising the Matlab engine for python, each matrix is printed to CLI for inspection.

"""
import logging
import math
import cmath
import matplotlib
import visa
from pyvisa import VisaIOError
import sys
import os
import time, datetime
import numpy
sys.path.append("..")
from matplotlib import pyplot as pp
import csv
import matlab.engine
import shutil
import stat
import matplotlib.pylab as pl 
import platform

from win32api import *
from win32gui import *
import win32con
import struct
import tkMessageBox
# from keras.models import Sequential

class WindowsBalloonTip:
    def __init__(self, title, msg):
        message_map = {
                win32con.WM_DESTROY: self.OnDestroy,
        }
        # Register the Window class.
        wc = WNDCLASS()
        hinst = wc.hInstance = GetModuleHandle(None)
        wc.lpszClassName = "PythonTaskbar"
        wc.lpfnWndProc = message_map # could also specify a wndproc.
        classAtom = RegisterClass(wc)
        # Create the Window.
        style = win32con.WS_OVERLAPPED | win32con.WS_SYSMENU
        self.hwnd = CreateWindow( classAtom, "Taskbar", style, \
                0, 0, win32con.CW_USEDEFAULT, win32con.CW_USEDEFAULT, \
                0, 0, hinst, None)
        UpdateWindow(self.hwnd)
        iconPathName = os.path.abspath(os.path.join( sys.path[0], "balloontip.ico" ))
        icon_flags = win32con.LR_LOADFROMFILE | win32con.LR_DEFAULTSIZE
        try:
           hicon = LoadImage(hinst, iconPathName, \
                    win32con.IMAGE_ICON, 0, 0, icon_flags)
        except:
          hicon = LoadIcon(0, win32con.IDI_APPLICATION)
        flags = NIF_ICON | NIF_MESSAGE | NIF_TIP
        nid = (self.hwnd, 0, flags, win32con.WM_USER+20, hicon, "tooltip")
        Shell_NotifyIcon(NIM_ADD, nid)
        Shell_NotifyIcon(NIM_MODIFY, \
                         (self.hwnd, 0, NIF_INFO, win32con.WM_USER+20,\
                          hicon, "Balloon  tooltip",msg,200,title))
        # self.show_balloon(title, msg)
        time.sleep(10)
        DestroyWindow(self.hwnd)
    def OnDestroy(self, hwnd, msg, wparam, lparam):
        nid = (self.hwnd, 0)
        Shell_NotifyIcon(NIM_DELETE, nid)
        PostQuitMessage(0) # Terminate the app.

# import smithplot
# from smithplot import SmithAxes

LEVELS = {'debug': logging.DEBUG,
          'info': logging.INFO,
          'warning': logging.WARNING,
          'error': logging.ERROR,
          'critical': logging.CRITICAL}

if len(sys.argv) > 1:
    level_name = sys.argv[1]
    level = LEVELS.get(level_name, logging.NOTSET)
    logging.basicConfig(level=level)

job = raw_input("What would you like to do? \n 1. Initilaise Analyser \n 2. Measure Calibration Standards \n 3. Run Calibration Algorithm \n 4. Calibrated Measurement \n")

def balloon_tip(title, msg):
    w=WindowsBalloonTip(title, msg)


def rmtree(top):
    for root, dirs, files in os.walk(top, topdown=False):
        for name in files:
            filename = os.path.join(root, name)
            os.chmod(filename, stat.S_IWUSR)
            os.remove(filename)
        for name in dirs:
            os.rmdir(os.path.join(root, name))
    os.rmdir(top)      

def MLtest():
    tf = eng.isprime(37)
    return tf

def send(msg): 
    smu.write(msg)
    time.sleep(0.02)    

def CreateDirectories (npts): 
    for n in range(npts):
        if not os.path.exists("Match{0}".format(n)):
                os.makedirs("Match{0}".format(n))
        if not os.path.exists("Match{0}CSV".format(n)):
                os.makedirs("Match{0}CSV".format(n))

        if not os.path.exists("ReflectMatch{0}".format(n)):
                os.makedirs("ReflectMatch{0}".format(n))
        if not os.path.exists("ReflectMatch{0}CSV".format(n)):
                os.makedirs("ReflectMatch{0}CSV".format(n))
        if not os.path.exists("MatchReflect{0}".format(n)):
                os.makedirs("MatchReflect{0}".format(n))
        if not os.path.exists("MatchReflect{0}CSV".format(n)):
                os.makedirs("MatchReflect{0}CSV".format(n))

    if not os.path.exists("Match1"):
            os.makedirs("Match1")
    if not os.path.exists("Match1CSV"):
            os.makedirs("Match1CSV")

    if not os.path.exists("Match2"):
            os.makedirs("Match2")
    if not os.path.exists("Match2CSV"):
            os.makedirs("Match2CSV")

    if not os.path.exists("Reflect"):
            os.makedirs("Reflect")
    if not os.path.exists("ReflectCSV"):
            os.makedirs("ReflectCSV")

    if not os.path.exists("Thru"):
            os.makedirs("Thru")
    if not os.path.exists("ThruCSV"):
            os.makedirs("ThruCSV")


                     
    # print(' Directories successfully created \n')

def CleanDirectories (npts): 
    print('{0}\Match{1}'.format(dir_path, npts))
    for n in range(npts):
        if not os.path.exists('{1}\Match{0}'.format(n, dir_path)):
                rmtree('{1}\Match{0}'.format(n, dir_path))
        if not os.path.exists('{1}\Match{0}CSV'.format(n, dir_path)):
                rmtree('{1}\Match{0}CSV'.format(n, dir_path))

        if not os.path.exists("ReflectMatch{0}".format(n)):
                shutil.rmtree("ReflectMatch{0}".format(n))
        if not os.path.exists("ReflectMatch{0}CSV".format(n)):
                shutil.rmtree("ReflectMatch{0}CSV".format(n))
        if not os.path.exists("MatchReflect{0}".format(n)):
                shutil.rmtree("MatchReflect{0}".format(n))
        if not os.path.exists("MatchReflect{0}CSV".format(n)):
                shutil.rmtree("MatchReflect{0}CSV".format(n))

    if not os.path.exists("Match1"):
            shutil.rmtree("Match1")
    if not os.path.exists("Match1CSV"):
            shutil.rmtree("Match1CSV")

    if not os.path.exists("Match2"):
            shutil.rmtree("Match2")
    if not os.path.exists("Match2CSV"):
            shutil.rmtree("Match2CSV")

    if not os.path.exists("Reflect"):
            shutil.rmtree("Reflect")
    if not os.path.exists("ReflectCSV"):
            shutil.rmtree("ReflectCSV")

    if not os.path.exists("Thru"):
            shutil.rmtree("Thru")
    if not os.path.exists("ThruCSV"):
            shutil.rmtree("ThruCSV")


                     
    print(' Directories successfully cleaned \n')

def MeasureS11 (standard):

    smu.write("MEAS S11")
    data = []

    RawOutput = smu.query("OUTPRAW%d?" % 1)
    data.extend(RawOutput)

    out_file_name = ("{0}\OutPutRawS11".format(standard))
    numpy.save(out_file_name, numpy.array(data))

    pairs = RawOutput.split(',')
    length = len(pairs)
    #print(pairs)
    for i in range(length):
        pairs[i] =float(pairs[i])
    data2 = []
    data2.extend(pairs)

    out_file_name = ("{0}\OutPutRawFloatS11".format(standard))
    numpy.save(out_file_name, numpy.array(data2))

    # Condition data: 
    # Data is returned in complex pairs, i.e Real, Imaginary, Real etc 
    # Magnitude [dB] = 20*log(sqrt(Real^2 + Imaginary^2))
    # Phase = arctan(Imaginary/Real)
    output = []
    ReOut = []
    ImOut =[]

    for i in range(len(data2)):
        if i % 2 == 0:
            Re = data2[i]
            Im = data2[i+1]
            # Cdata = (cmath.polar(complex(Re, Im)))
            # output.append(Re, Im)
            magphi = [Re, Im]
            Re = [Re, ]
            Im = [Im, ]

            output.append(magphi)
            ReOut.append(Re)
            ImOut.append(Im)

    # CSV file write, format is ??? this is then read back and plotted in a smith chart. 
    out_file_name = ("{}\OutPutMagPhiS11".format(standard))
    csv_out_file_name = ("{}CSV\OutPutMagPhiS11.csv".format(standard))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(output))
        for row in output:
            spamwriter.writerow(row)
    out_file_name = ("{}\OutPutMagS11".format(standard))
    csv_out_file_name = ("{}CSV\OutPutMagS11.csv".format(standard))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=' ', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(ReOut))
        for row in ReOut:
            spamwriter.writerow(row)
    out_file_name = ("{}\OutPutPhiS11".format(standard))
    csv_out_file_name = ("{}CSV\OutPutPhiS11.csv".format(standard))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=' ', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(ImOut))
        for row in ImOut:
            spamwriter.writerow(row)  

def MeasureS12 (standard): 
    smu.write("MEAS S12")
    data = []

    RawOutput = smu.query("OUTPRAW%d?" % 1)
    data.extend(RawOutput)

    out_file_name = ("{}\OutPutRawS12".format(standard))
    numpy.save(out_file_name, numpy.array(data))

    pairs = RawOutput.split(',')
    length = len(pairs)
    #print(pairs)
    for i in range(length):
        pairs[i] =float(pairs[i])
    data2 = []
    data2.extend(pairs)

    out_file_name = ("{}\OutPutRawFloatS12".format(standard))
    numpy.save(out_file_name, numpy.array(data2))

    # Condition data: 
    # Data is returned in complex pairs, i.e Real, Imaginary, Real etc 
    # Magnitude [dB] = 20*log(sqrt(Real^2 + Imaginary^2))
    # Phase = arctan(Imaginary/Real)
    output = []
    ReOut = []
    ImOut =[]

    for i in range(len(data2)):
        if i % 2 == 0:
            Re = data2[i]
            Im = data2[i+1]
            # Cdata = (cmath.polar(complex(Re, Im)))
            # output.append(Re, Im)
            magphi = [Re, Im]
            Re = [Re, ]
            Im = [Im, ]

            output.append(magphi)
            ReOut.append(Re)
            ImOut.append(Im)

    # CSV file write, format is ??? this is then read back and plotted in a smith chart. 
    out_file_name = ("{}\OutPutMagPhiS12".format(standard))
    csv_out_file_name = ("{}CSV\OutPutMagPhiS12.csv".format(standard))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(output))
        for row in output:
            spamwriter.writerow(row)
    out_file_name = ("{}\OutPutMagS12".format(standard))
    csv_out_file_name = ("{}CSV\OutPutMagS12.csv".format(standard))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=' ', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(ReOut))
        for row in ReOut:
            spamwriter.writerow(row)
    out_file_name = ("{}\OutPutPhiS12".format(standard))
    csv_out_file_name = ("{}CSV\OutPutPhiS12.csv".format(standard))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=' ', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(ImOut))
        for row in ImOut:
            spamwriter.writerow(row)           
    
def MeasureS21 (standard): 
    smu.write("MEAS S21")
    data = []

    RawOutput = smu.query("OUTPRAW%d?" % 1)
    data.extend(RawOutput)

    out_file_name = ("{}\OutPutRawS21".format(standard))
    numpy.save(out_file_name, numpy.array(data))

    pairs = RawOutput.split(',')
    length = len(pairs)
    #print(pairs)
    for i in range(length):
        pairs[i] =float(pairs[i])
    data2 = []
    data2.extend(pairs)

    out_file_name = ("{}\OutPutRawFloatS21".format(standard))
    numpy.save(out_file_name, numpy.array(data2))

    # Condition data: 
    # Data is returned in complex pairs, i.e Real, Imaginary, Real etc 
    # Magnitude [dB] = 20*log(sqrt(Real^2 + Imaginary^2))
    # Phase = arctan(Imaginary/Real)
    output = []
    ReOut = []
    ImOut =[]

    for i in range(len(data2)):
        if i % 2 == 0:
            Re = data2[i]
            Im = data2[i+1]
            # Cdata = (cmath.polar(complex(Re, Im)))
            # output.append(Re, Im)
            magphi = [Re, Im]
            Re = [Re, ]
            Im = [Im, ]

            output.append(magphi)
            ReOut.append(Re)
            ImOut.append(Im)

    # CSV file write, format is ??? this is then read back and plotted in a smith chart. 
    out_file_name = ("{}\OutPutMagPhiS21".format(standard))
    csv_out_file_name = ("{}CSV\OutPutMagPhiS21.csv".format(standard))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(output))
        for row in output:
            spamwriter.writerow(row)
    out_file_name = ("{}\OutPutMagPhiS21".format(standard))
    csv_out_file_name = ("{}CSV\OutPutMagS21.csv".format(standard))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=' ', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(ReOut))
        for row in ReOut:
            spamwriter.writerow(row)
    out_file_name = ("{}\OutPutPhiS21".format(standard))
    csv_out_file_name = ("{}CSV\OutPutPhiS21.csv".format(standard))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=' ', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(ImOut))
        for row in ImOut:
            spamwriter.writerow(row)           
      
def MeasureS22 (standard): 
    
    smu.write("MEAS S22")
    data = []

    RawOutput = smu.query("OUTPRAW%d?" % 1)
    data.extend(RawOutput)

    out_file_name = ("{}\OutPutRawS22".format(standard))
    numpy.save(out_file_name, numpy.array(data))

    pairs = RawOutput.split(',')
    length = len(pairs)
    #print(pairs)
    for i in range(length):
        pairs[i] =float(pairs[i])
    data2 = []
    data2.extend(pairs)

    out_file_name = ("{}\OutPutRawFloatS22".format(standard))
    numpy.save(out_file_name, numpy.array(data2))

    # Condition data: 
    # Data is returned in complex pairs, i.e Real, Imaginary, Real etc 
    # Magnitude [dB] = 20*log(sqrt(Real^2 + Imaginary^2))
    # Phase = arctan(Imaginary/Real)
    output = []
    ReOut = []
    ImOut =[]

    for i in range(len(data2)):
        if i % 2 == 0:
            Re = data2[i]
            Im = data2[i+1]
            # Cdata = (cmath.polar(complex(Re, Im)))
            # output.append(Re, Im)
            magphi = [Re, Im]
            Re = [Re, ]
            Im = [Im, ]

            output.append(magphi)
            ReOut.append(Re)
            ImOut.append(Im)

    # CSV file write, format is ??? this is then read back and plotted in a smith chart. 
    out_file_name = ("{}\OutPutMagPhiS22".format(standard))
    csv_out_file_name = ("{}CSV\OutPutMagPhiS22.csv".format(standard))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(output))
        for row in output:
            spamwriter.writerow(row)
    out_file_name = ("{}\OutPutMagPhiS22".format(standard))
    csv_out_file_name = ("{}CSV\OutPutMagS22.csv".format(standard))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=' ', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(ReOut))
        for row in ReOut:
            spamwriter.writerow(row)
    out_file_name = ("{}\OutPutPhiS22".format(standard))
    csv_out_file_name = ("{}CSV\OutPutPhiS22.csv".format(standard))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=' ', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(ImOut))
        for row in ImOut:
            spamwriter.writerow(row)           

def slidingLoad(npts): 
    ports = 2
    print(' SLIDING LOAD - Match Standard')
    print(' Ports 1 & 2 are measured simultaneously, but each port is to be slid one at a time. \n i.e The measurement sequence is:')
    print(' Port 1 | Port 2 \n  0     |   0 \n  0     |   1 \n  0     |   2 \n  0     |   3 \n  1     |   0 \n  2     |   0 \n  3     |   0 \n \n Position 0 should be chosen such that the loads can be returned there easily (approximately) \n')
    for port in xrange(ports):
        port = port + 1
        raw_input('Press enter when ready to measure port {0} for the sliding load standard \n'.format(port))
        for n in xrange(npts):
            raw_input('Press enter when ready to measure position {0} on port {1} for the sliding load standard \n'.format(n, port))
            print(port)
            print(n)
            smu.write("MEAS S11")
            time.sleep(60)  
            MeasureS11('Match{0}{1}'.format(n, port))
            print('done s11')

            smu.write("MEAS S21")
            time.sleep(60)
            MeasureS21('Match{0}{1}'.format(n, port))
            print('done s21')

            smu.write("MEAS S12")
            time.sleep(60)
            MeasureS12('Match{0}{1}'.format(n, port))
            print('done s12')

            smu.write("MEAS S22")
            time.sleep(60)
            MeasureS22('Match{0}{1}'.format(n, port))
            print("done s22")

def Thru (): 
    print(' THRU - 0 length thru standard')
    print(' Ports 1 & 2 are measured simultaneously')
    raw_input(' Press enter when ready \n')
    
    smu.write("MEAS S21")
    time.sleep(120)
    MeasureS21('Thru')
    time.sleep(10)
    print("s21")
    timeelapsed = datetime.datetime.now().time()
    print(timeelapsed)

    # smu.write("MEAS S22")
    # time.sleep(120)
    # MeasureS22('Thru')
    # time.sleep(10)
    # print("s22")
    # timeelapsed = datetime.datetime.now().time()
    # print(timeelapsed)

    smu.write("MEAS S11")
    time.sleep(120)
    MeasureS11('Thru')
    time.sleep(10)
    print("s11")
    timeelapsed = datetime.datetime.now().time()
    print(timeelapsed)

    # smu.write("MEAS S12")
    # time.sleep(120)
    # MeasureS12('Thru')
    # time.sleep(10)
    # print("s12")
    # timeelapsed = datetime.datetime.now().time()
    # print(timeelapsed)



    # clear()

def Reflect ():
    print(' REFLECT ')
    print(' Ports 1 & 2 are measured simultaneously')
    raw_input(' Press enter when ready \n')
    print(' Measuring...')

    # smu.write("MEAS S12")
    # time.sleep(120)
    # MeasureS12('Reflect')
    # time.sleep(60)
    # print("s12")
    # timeelapsed = datetime.datetime.now().time()
    # print(timeelapsed)

    smu.write("MEAS S11")
    time.sleep(20)
    MeasureS11('Reflect')
    time.sleep(60)
    print("s11")
    timeelapsed = datetime.datetime.now().time()
    print(timeelapsed)

    # smu.write("MEAS S22")
    # time.sleep(20)
    # MeasureS22('Reflect')
    # time.sleep(60)
    # print("s22")
    # timeelapsed = datetime.datetime.now().time()
    # print(timeelapsed)

    # smu.write("MEAS S21")
    # time.sleep(120)
    # MeasureS21('Reflect')
    # time.sleep(60)
    # print("s21")
    # timeelapsed = datetime.datetime.now().time()
    # print(timeelapsed)

    # clear()

def ReflectMatch (npts):
    print(' SLIDING LOAD - Reflect-Match Standard')
    print(' Ports 1 & 2 are measured simultaneously, but port 2 is to be a sliding load measured at positions one at a time. \n')
    for n in xrange(npts):    
        raw_input('Press enter when ready to measure position {0} for the sliding load standard in the Reflect Match standard \n'.format(n))
        smu.write("MEAS S11")
        time.sleep(60)  
        MeasureS11('ReflectMatch{0}'.format(n))
        MeasureS11('ReflectMatch')
        print('done s11')

        smu.write("MEAS S21")        
        time.sleep(60)  
        MeasureS21('ReflectMatch{0}'.format(n))
        MeasureS21('ReflectMatch')
        print('done s12')

        smu.write("MEAS S12")
        time.sleep(60)  
        MeasureS12('ReflectMatch{0}'.format(n))
        MeasureS12('ReflectMatch')
        print('done s21')

        smu.write("MEAS S22")
        time.sleep(60)
        MeasureS22('ReflectMatch{0}'.format(n))
        MeasureS22('ReflectMatch')        
        print('done s22')

def MatchReflect (npts):
    print(' SLIDING LOAD - Match-Reflect Standard')
    print(' Ports 1 & 2 are measured simultaneously, but port 1 is to be a sliding load measured at positions one at a time. \n')
    
    for n in xrange(npts):    
        raw_input('Press enter when ready to measure position {0} for the sliding load standard in the Match Reflect standard \n'.format(n))
        smu.write("MEAS S11")
        time.sleep(60)  
        MeasureS11('MatchReflect{0}'.format(n))
        MeasureS11('MatchReflect')
        print('done s11')

        smu.write("MEAS S21")        
        time.sleep(60)  
        MeasureS21('MatchReflect{0}'.format(n))
        MeasureS21('MatchReflect')
        print('done s12')

        smu.write("MEAS S12")
        time.sleep(60)  
        MeasureS12('MatchReflect{0}'.format(n))
        MeasureS12('MatchReflect')
        print('done s21')

        smu.write("MEAS S22")
        time.sleep(60)
        MeasureS22('MatchReflect{0}'.format(n))
        MeasureS22('MatchReflect')        
        print('done s22')

def GetDevice():
    rm = visa.ResourceManager()
    resources = rm.list_resources()
    print(resources)    

    smu = rm.open_resource('TCPIP0::130.217.46.140::gpib0,9::INSTR') #'TCPIP0::130.217.188.202::gpib0,9::INSTR'

    try:
        # If connection is succesful, print the IDN
        IDN = smu.query("*IDN?")
        if IDN != '':
            print(" Instrument connected successfully: \n " + IDN)
    except VisaIOError:
        # If connection times out, show user list of available GPIB addresses. Then exit
        print("Connection timed out.")
        print("Available resources are: ")
        print(resources)
        quit()

    return smu

def matrixprint(matrix): 
    line = []
    for row in matrix:
        for val in row:
            line.append(val)
        line = ("{0.real:.3}+{0.imag:.3}j".format(i) for i in line) 
        line = ("{0:>10}".format(i) for i in line)
        line = str(list(line))
        print(line.strip()) 
        line = []
    print('\n')

def MatchCirclefit(supress):
    d = {}
    sweep = 0
    abRS11 = []
    abRS12 = []
    abRS21 = []
    abRS22 = []
    ports = 2

    for port in range(ports):
        port = port + 1
        for n in range(npts):

            with open('{1}\Match{0}{2}CSV\OutPutMagPhiS11.csv'.format(n, dir_path, port), "rb") as f:
                reader = csv.reader(f, delimiter=",")
                for freq, line in enumerate(reader):
                    d['S11pos{0}freq{1}'.format(n,freq)] = line
                    sweep = freq

            with open('{1}\Match{0}{2}CSV\OutPutMagPhiS12.csv'.format(n, dir_path, port), "rb") as f:
                reader = csv.reader(f, delimiter=",")
                for freq, line in enumerate(reader):
                    d['S12pos{0}freq{1}'.format(n,freq)] = line

            with open('{1}\Match{0}{2}CSV\OutPutMagPhiS21.csv'.format(n, dir_path, port), "rb") as f:
                reader = csv.reader(f, delimiter=",")
                for freq, line in enumerate(reader):
                    d['S21pos{0}freq{1}'.format(n,freq)] = line

            with open('{1}\Match{0}{2}CSV\OutPutMagPhiS22.csv'.format(n, dir_path, port), "rb") as f:
                reader = csv.reader(f, delimiter=",")
                for freq, line in enumerate(reader):
                    d['S22pos{0}freq{1}'.format(n,freq)] = line


        for freq in range(sweep):
            pointsS11 = []
            pointsS12 = []
            pointsS21 = []
            pointsS22 = []

            for n in range(npts):    
                pointsS11.append(d['S11pos{0}freq{1}'.format(n,freq)]) 
                out_file_name = ('S11freq{0}positions'.format(freq))
                csv_out_file_name = ("S11freq{0}positions.csv".format(freq))
            with open(csv_out_file_name, 'w') as csvfile:
                spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                            
                numpy.save(out_file_name, numpy.array(pointsS11))
                for row in pointsS11:
                    spamwriter.writerow(row)

            for n in range(npts):    
                pointsS22.append(d['S22pos{0}freq{1}'.format(n,freq)]) 
                out_file_name = ('S22freq{0}positions'.format(freq))
                csv_out_file_name = ("S22freq{0}positions.csv".format(freq))
            with open(csv_out_file_name, 'w') as csvfile:
                spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                            
                numpy.save(out_file_name, numpy.array(pointsS22))
                for row in pointsS22:
                    spamwriter.writerow(row)

            for n in range(npts):    
                pointsS12.append(d['S12pos{0}freq{1}'.format(n,freq)]) 
                out_file_name = ('S12freq{0}positions'.format(freq))
                csv_out_file_name = ("S12freq{0}positions.csv".format(freq))
            with open(csv_out_file_name, 'w') as csvfile:
                spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                            
                numpy.save(out_file_name, numpy.array(pointsS12))
                for row in pointsS12:
                    spamwriter.writerow(row)

            for n in range(npts):    
                pointsS21.append(d['S21pos{0}freq{1}'.format(n,freq)]) 
                out_file_name = ('S21freq{0}positions'.format(freq))
                csv_out_file_name = ("S21freq{0}positions.csv".format(freq))
            with open(csv_out_file_name, 'w') as csvfile:
                spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                            
                numpy.save(out_file_name, numpy.array(pointsS21))
                for row in pointsS21:
                    spamwriter.writerow(row)

            eng.workspace['pointsS11'] = eng.csvread('S11freq{0}positions.csv'.format(freq))
            eng.workspace['abR'] = eng.eval('CircleFitByTaubin{0}(pointsS11)'.format(supress))
            abR = eng.workspace['abR']
            eng.workspace['a'] = abR[0][0]
            eng.workspace['b'] = abR[0][1]
            eng.workspace['R'] = abR[0][2]
            eng.workspace['centers'] =  eng.eval('[a b]')
            Sxx = 11
            eng.workspace['f'] = eng.eval('pythonplottingtool(a,b,R,pointsS11,{0},{1})'.format(freq, Sxx))
            abRS11.append(eng.workspace['abR'])

            eng.workspace['pointsS12'] = eng.csvread('S12freq{0}positions.csv'.format(freq))
            eng.workspace['abR'] = eng.eval('CircleFitByTaubin{0}(pointsS12)'.format(supress))
            abR = eng.workspace['abR']
            eng.workspace['a'] = abR[0][0]
            eng.workspace['b'] = abR[0][1]
            eng.workspace['R'] = abR[0][2]
            eng.workspace['centers'] =  eng.eval('[a b]')
            Sxx = 12
            eng.workspace['f'] = eng.eval('pythonplottingtool(a,b,R,pointsS12,{0},{1})'.format(freq, Sxx))
            abRS12.append(eng.workspace['abR'])
        
            eng.workspace['pointsS21'] = eng.csvread('S21freq{0}positions.csv'.format(freq))
            eng.workspace['abR'] = eng.eval('CircleFitByTaubin{0}(pointsS21)'.format(supress))
            abR = eng.workspace['abR']
            eng.workspace['a'] = abR[0][0]
            eng.workspace['b'] = abR[0][1]
            eng.workspace['R'] = abR[0][2]
            eng.workspace['centers'] =  eng.eval('[a b]')
            Sxx = 21
            eng.workspace['f'] = eng.eval('pythonplottingtool(a,b,R,pointsS21,{0},{1})'.format(freq, Sxx))
            abRS21.append(eng.workspace['abR'])

            eng.workspace['pointsS22'] = eng.csvread('S22freq{0}positions.csv'.format(freq))
            eng.workspace['abR'] = eng.eval('CircleFitByTaubin{0}(pointsS22)'.format(supress))
            abR = eng.workspace['abR']
            eng.workspace['a'] = abR[0][0]
            eng.workspace['b'] = abR[0][1]
            eng.workspace['R'] = abR[0][2]
            eng.workspace['centers'] =  eng.eval('[a b]')
            Sxx = 22
            eng.workspace['f'] = eng.eval('pythonplottingtool(a,b,R,pointsS22,{0},{1})'.format(freq, Sxx))
            abRS22.append(eng.workspace['abR'])

        out_file_name = ('M_S11CirclefitPort{0}'.format(port))
        csv_out_file_name = ("M_S11Circlefit{0}.csv".format(port))
        with open(csv_out_file_name, 'w') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                        
            numpy.save(out_file_name, numpy.array(abRS11))
            for row in abRS11:
                spamwriter.writerow(row)

        out_file_name = ('M_S12CirclefitPort{0}'.format(port))
        csv_out_file_name = ("M_S12Circlefit{0}.csv".format(port))
        with open(csv_out_file_name, 'w') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                        
            numpy.save(out_file_name, numpy.array(abRS12))
            for row in abRS12:
                spamwriter.writerow(row)
        
        out_file_name = ('M_S21CirclefitPort{0}'.format(port))
        csv_out_file_name = ("M_S21Circlefit{0}.csv".format(port))
        with open(csv_out_file_name, 'w') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                        
            numpy.save(out_file_name, numpy.array(abRS21))
            for row in abRS21:
                spamwriter.writerow(row)
        
        out_file_name = ('M_S22CirclefitPort{0}'.format(port))
        csv_out_file_name = ("M_S22Circlefit{0}.csv".format(port))
        with open(csv_out_file_name, 'w') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                        
            numpy.save(out_file_name, numpy.array(abRS22))
            for row in abRS22:
                spamwriter.writerow(row)

def MRCirclefit(supress):
    d = {}
    sweep = 0
    abRS11 = []
    abRS12 = []
    abRS21 = []
    abRS22 = []

    for n in range(npts):

        with open('{1}\MatchReflect{0}CSV\OutPutMagPhiS11.csv'.format(n, dir_path), "rb") as f:
            reader = csv.reader(f, delimiter=",")
            for freq, line in enumerate(reader):
                d['S11pos{0}freq{1}'.format(n,freq)] = line
                sweep = freq

        with open('{1}\MatchReflect{0}CSV\OutPutMagPhiS12.csv'.format(n, dir_path), "rb") as f:
            reader = csv.reader(f, delimiter=",")
            for freq, line in enumerate(reader):
                d['S12pos{0}freq{1}'.format(n,freq)] = line

        with open('{1}\MatchReflect{0}CSV\OutPutMagPhiS21.csv'.format(n, dir_path), "rb") as f:
            reader = csv.reader(f, delimiter=",")
            for freq, line in enumerate(reader):
                d['S21pos{0}freq{1}'.format(n,freq)] = line

        with open('{1}\MatchReflect{0}CSV\OutPutMagPhiS22.csv'.format(n, dir_path), "rb") as f:
            reader = csv.reader(f, delimiter=",")
            for freq, line in enumerate(reader):
                d['S22pos{0}freq{1}'.format(n,freq)] = line


    for freq in range(sweep):
        pointsS11 = []
        pointsS12 = []
        pointsS21 = []
        pointsS22 = []

        for n in range(npts):    
            pointsS11.append(d['S11pos{0}freq{1}'.format(n,freq)]) 
            out_file_name = ('S11freq{0}positions'.format(freq))
            csv_out_file_name = ("S11freq{0}positions.csv".format(freq))
        with open(csv_out_file_name, 'w') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                        
            numpy.save(out_file_name, numpy.array(pointsS11))
            for row in pointsS11:
                spamwriter.writerow(row)

        for n in range(npts):    
            pointsS22.append(d['S22pos{0}freq{1}'.format(n,freq)]) 
            out_file_name = ('S22freq{0}positions'.format(freq))
            csv_out_file_name = ("S22freq{0}positions.csv".format(freq))
        with open(csv_out_file_name, 'w') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                        
            numpy.save(out_file_name, numpy.array(pointsS22))
            for row in pointsS22:
                spamwriter.writerow(row)

        for n in range(npts):    
            pointsS12.append(d['S12pos{0}freq{1}'.format(n,freq)]) 
            out_file_name = ('S12freq{0}positions'.format(freq))
            csv_out_file_name = ("S12freq{0}positions.csv".format(freq))
        with open(csv_out_file_name, 'w') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                        
            numpy.save(out_file_name, numpy.array(pointsS12))
            for row in pointsS12:
                spamwriter.writerow(row)

        for n in range(npts):    
            pointsS21.append(d['S21pos{0}freq{1}'.format(n,freq)]) 
            out_file_name = ('S21freq{0}positions'.format(freq))
            csv_out_file_name = ("S21freq{0}positions.csv".format(freq))
        with open(csv_out_file_name, 'w') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                        
            numpy.save(out_file_name, numpy.array(pointsS21))
            for row in pointsS21:
                spamwriter.writerow(row)


        eng.workspace['pointsS11'] = eng.csvread('S11freq{0}positions.csv'.format(freq))
        eng.workspace['abR'] = eng.eval('CircleFitByTaubin{0}(pointsS11)'.format(supress))
        abRS11.append(eng.workspace['abR'])

        eng.workspace['pointsS12'] = eng.csvread('S12freq{0}positions.csv'.format(freq))
        eng.workspace['abR'] = eng.eval('CircleFitByTaubin{0}(pointsS12)'.format(supress))
        abRS12.append(eng.workspace['abR'])
    
        eng.workspace['pointsS21'] = eng.csvread('S21freq{0}positions.csv'.format(freq))
        eng.workspace['abR'] = eng.eval('CircleFitByTaubin{0}(pointsS21)'.format(supress))
        abRS21.append(eng.workspace['abR'])

        eng.workspace['pointsS22'] = eng.csvread('S22freq{0}positions.csv'.format(freq))
        eng.workspace['abR'] = eng.eval('CircleFitByTaubin{0}(pointsS22)'.format(supress))
        abRS22.append(eng.workspace['abR'])

    out_file_name = ('MR_S11Circlefit')
    csv_out_file_name = ("MR_S11Circlefitfre.csv".format(freq))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(abRS11))
        for row in abRS11:
            spamwriter.writerow(row)

    out_file_name = ('MR_S12Circlefit')
    csv_out_file_name = ("MR_S12Circlefitfre.csv".format(freq))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(abRS12))
        for row in abRS12:
            spamwriter.writerow(row)
    
    out_file_name = ('MR_S21Circlefit')
    csv_out_file_name = ("MR_S21Circlefitfre.csv".format(freq))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(abRS21))
        for row in abRS21:
            spamwriter.writerow(row)
    
    out_file_name = ('MR_S22Circlefit')
    csv_out_file_name = ("MR_S22Circlefitfre.csv".format(freq))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(abRS22))
        for row in abRS22:
            spamwriter.writerow(row)

def RMCirclefit(supress):
    d = {}
    sweep = 0
    abRS11 = []
    abRS12 = []
    abRS21 = []
    abRS22 = []

    for n in range(npts):

        with open('{1}\ReflectMatch{0}CSV\OutPutMagPhiS11.csv'.format(n, dir_path), "rb") as f:
            reader = csv.reader(f, delimiter=",")
            for freq, line in enumerate(reader):
                d['S11pos{0}freq{1}'.format(n,freq)] = line
                sweep = freq

        with open('{1}\ReflectMatch{0}CSV\OutPutMagPhiS12.csv'.format(n, dir_path), "rb") as f:
            reader = csv.reader(f, delimiter=",")
            for freq, line in enumerate(reader):
                d['S12pos{0}freq{1}'.format(n,freq)] = line

        with open('{1}\ReflectMatch{0}CSV\OutPutMagPhiS21.csv'.format(n, dir_path), "rb") as f:
            reader = csv.reader(f, delimiter=",")
            for freq, line in enumerate(reader):
                d['S21pos{0}freq{1}'.format(n,freq)] = line

        with open('{1}\ReflectMatch{0}CSV\OutPutMagPhiS22.csv'.format(n, dir_path), "rb") as f:
            reader = csv.reader(f, delimiter=",")
            for freq, line in enumerate(reader):
                d['S22pos{0}freq{1}'.format(n,freq)] = line


    for freq in range(sweep):
        pointsS11 = []
        pointsS12 = []
        pointsS21 = []
        pointsS22 = []

        for n in range(npts):    
            pointsS11.append(d['S11pos{0}freq{1}'.format(n,freq)]) 
            out_file_name = ('S11freq{0}positions'.format(freq))
            csv_out_file_name = ("S11freq{0}positions.csv".format(freq))
        with open(csv_out_file_name, 'w') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                        
            numpy.save(out_file_name, numpy.array(pointsS11))
            for row in pointsS11:
                spamwriter.writerow(row)

        for n in range(npts):    
            pointsS22.append(d['S22pos{0}freq{1}'.format(n,freq)]) 
            out_file_name = ('S22freq{0}positions'.format(freq))
            csv_out_file_name = ("S22freq{0}positions.csv".format(freq))
        with open(csv_out_file_name, 'w') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                        
            numpy.save(out_file_name, numpy.array(pointsS22))
            for row in pointsS22:
                spamwriter.writerow(row)

        for n in range(npts):    
            pointsS12.append(d['S12pos{0}freq{1}'.format(n,freq)]) 
            out_file_name = ('S12freq{0}positions'.format(freq))
            csv_out_file_name = ("S12freq{0}positions.csv".format(freq))
        with open(csv_out_file_name, 'w') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                        
            numpy.save(out_file_name, numpy.array(pointsS12))
            for row in pointsS12:
                spamwriter.writerow(row)

        for n in range(npts):    
            pointsS21.append(d['S21pos{0}freq{1}'.format(n,freq)]) 
            out_file_name = ('S21freq{0}positions'.format(freq))
            csv_out_file_name = ("S21freq{0}positions.csv".format(freq))
        with open(csv_out_file_name, 'w') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                        
            numpy.save(out_file_name, numpy.array(pointsS21))
            for row in pointsS21:
                spamwriter.writerow(row)

        eng.workspace['pointsS11'] = eng.csvread('S11freq{0}positions.csv'.format(freq))
        eng.workspace['abR'] = eng.eval('CircleFitByTaubin{0}(pointsS11)'.format(supress))
        abRS11.append(eng.workspace['abR'])

        eng.workspace['pointsS12'] = eng.csvread('S12freq{0}positions.csv'.format(freq))
        eng.workspace['abR'] = eng.eval('CircleFitByTaubin{0}(pointsS12)'.format(supress))
        abRS12.append(eng.workspace['abR'])
    
        eng.workspace['pointsS21'] = eng.csvread('S21freq{0}positions.csv'.format(freq))
        eng.workspace['abR'] = eng.eval('CircleFitByTaubin{0}(pointsS21)'.format(supress))
        abRS21.append(eng.workspace['abR'])

        eng.workspace['pointsS22'] = eng.csvread('S22freq{0}positions.csv'.format(freq))
        eng.workspace['abR'] = eng.eval('CircleFitByTaubin{0}(pointsS22)'.format(supress))
        abRS22.append(eng.workspace['abR'])

    out_file_name = ('RM_S11Circlefit')
    csv_out_file_name = ("RM_S11Circlefitfre.csv".format(freq))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(abRS11))
        for row in abRS11:
            spamwriter.writerow(row)

    out_file_name = ('RM_S12Circlefit')
    csv_out_file_name = ("RM_S12Circlefitfre.csv".format(freq))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(abRS12))
        for row in abRS12:
            spamwriter.writerow(row)
    
    out_file_name = ('RM_S21Circlefit')
    csv_out_file_name = ("RM_S21Circlefitfre.csv".format(freq))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(abRS21))
        for row in abRS21:
            spamwriter.writerow(row)
    
    out_file_name = ('RM_S22Circlefit')
    csv_out_file_name = ("RM_S22Circlefitfre.csv".format(freq))
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(abRS22))
        for row in abRS22:
            spamwriter.writerow(row)
    return sweep

def HP4395A_Init(smu):
    smu.write("SWPT LINF")
    smu.query("*OPC?")
    smu.write("POIN 51")
    smu.query("*OPC?")
    smu.write("STOP 20000HZ")
    smu.query("*OPC?")
    smu.write("STAR 10000HZ")
    smu.query("*OPC?")
    smu.write("PORT1 280US")
    smu.query("*OPC?")
    smu.write("PORT2 280US")
    smu.write("PORE ON")
    smu.write("POWE -20")
    smu.write("BWAUTO OFF")
    smu.write("BW 2")

    POWER = smu.query("POWE?")
    port1extension = smu.query("PORT1?")
    smu.query("*OPC?")
    port2extension = smu.query("PORT2?")
    smu.query("*OPC?")
    sweeptype = smu.query("SWPT?")
    smu.query("*OPC?")
    stopfreq = smu.query("STOP?")
    smu.query("*OPC?")
    startfreq = smu.query("STAR?")
    smu.query("*OPC?")
    sweepres = smu.query("POIN?")
    IFBW = smu.query("BW?")

    IFBW = IFBW.rstrip()
    POWER = POWER.rstrip()
    port1extension = port1extension.rstrip()
    port2extension = port2extension.rstrip()
    sweeptype = sweeptype.rstrip()
    stopfreq = stopfreq.rstrip()
    startfreq = startfreq.rstrip()
    sweepres = sweepres.rstrip()

    print(" The IF BW is set to: {0} Hz".format(IFBW))
    print(" The source power is set to {0} dBm".format(POWER))
    print(" The extension for port 1 is: {0} s".format(port1extension))
    print(" The extension for port 2 is: {0} s".format(port2extension))
    print(" Sweep type is: {0} ".format(sweeptype))
    print(" Sweep end frequency: {0} Hz".format(stopfreq))
    print(" Sweep start frequency: {0} Hz".format(startfreq))
    print(" There are {0} points per sweep. \n".format(sweepres))

try:
    npts = raw_input(' How many points would you like to measure for the match standards? \n Defaults to a minimum of 3: ')
    npts = int(npts)
    if npts >= 3:    
        npts = npts
    else: 
        npts = 3

except:
    npts = 3
    print('\n Entry not valid, using default number of points \n')

try:
    supress = raw_input(' Would you like to suppress errors/warnings from the circle fitting method? y/n \n defaults to suppression: ')
    supress = str(supress)
    if supress.lower() in ['y', 'yes']:
        supressflag = 1
    if supress.lower() in ['n', 'no']:
        supressflag = 0
    else: 
        supressflag = 1   
except:
    print(' Entry not valid, using default suppression')
    supressflag = 1

# check matplotlib version requirement
if matplotlib.__version__ < '1.2':
    raise ImportError(" pySmithPlot requires at least matplotlib version 1.2")

dir_path = os.path.dirname(os.path.realpath(__file__))
clear = lambda: os.system('cls')
print('\n starting...... ')
print(' Loading matlab engine \n')
eng = matlab.engine.start_matlab()


try: 
    t = MLtest()
    if t == 1:
        result = 'Matlab engine successfully loaded. \n '
        print(result)
except:
    result = ' Matlab engine failed \n'
    print(result)
    sys.exit(1)

print("\n Python version and system info: \n {0}. \n Operating System {1} \n network name: {2}. \n {3}".format(sys.version, platform.system(), platform.node(), result))
print('\n')


if int(job) == 1:
    smu = GetDevice()
    HP4395A_Init(smu)

try:
    if int(job) == 2:

        smu = GetDevice()
        HP4395A_Init(smu)

        CreateDirectories(int(npts))
        # smu.write("MEAS S11")
        Thru() #Done
        # Reflect() #Done
        # slidingLoad(int(npts)) #Done

        # ReflectMatch(int(npts))
        # MatchReflect(int(npts))
        # CleanDirectories(int(npts))
        # MatchCirclefit(supressflag)
        # MRCirclefit(supressflag)
        # sweep = RMCirclefit(supressflag)

        balloon_tip("AVNA Job", "Measurement of Standards Complete.")

except:
    balloon_tip("AVNA Job", "Measurement of Standards Failed!")
    ctypes.windll.user32.MessageBoxW(0, u"Error", u"Error", 0)

# try:
if int(job) == 3: 

    print(' Starting Cal Algorithm')

    """
    Format all data into S parameter matrices ready for cal algorithm
    """
    supressflag = 1   
    sweep = RMCirclefit(supressflag)

    for freq in xrange(sweep): 
        for std in range(3):
            if std == 0: 
                S11 = numpy.load('M_S11CirclefitPort1.npy')
                S12 = numpy.load('M_S12CirclefitPort1.npy')
                S21 = numpy.load('M_S21CirclefitPort1.npy')
                S22 = numpy.load('M_S22CirclefitPort1.npy')
                
                # print(S11)

                ReS11 = S11[freq][0][0]
                ImS11 = S11[freq][0][1]
                ReS12 = S12[freq][0][0]
                ImS12 = S12[freq][0][1]
                ReS21 = S21[freq][0][0]
                ImS21 = S21[freq][0][1]
                ReS22 = S22[freq][0][0]
                ImS22 = S22[freq][0][1]   
                
                S_mMag = matlab.double([[ReS11,ReS12], [ReS21,ReS22]]) 
                S_mPhi = matlab.double([[ImS11,ImS12], [ImS21,ImS22]])
                eng.workspace['Mb'] = eng.complex(S_mMag, S_mPhi)
            
            if std == 1:
                S11 = numpy.load('RM_S11Circlefit.npy')
                S12 = numpy.load('RM_S12Circlefit.npy')
                S21 = numpy.load('RM_S21Circlefit.npy')
                S22 = numpy.load('RM_S22Circlefit.npy')

                ReS11 = S11[freq][0][0]
                ImS11 = S11[freq][0][1]
                ReS12 = S12[freq][0][0]
                ImS12 = S12[freq][0][1]
                ReS21 = S21[freq][0][0]
                ImS21 = S21[freq][0][1]
                ReS22 = S22[freq][0][0]
                ImS22 = S22[freq][0][1]
        
                S_mMag = matlab.double([[ReS11,ReS12], [ReS21,ReS22]]) 
                S_mPhi = matlab.double([[ImS11,ImS12], [ImS21,ImS22]])
                eng.workspace['Md'] = eng.complex(S_mMag, S_mPhi)
            
            if std == 2:
                S11 = numpy.load('MR_S11Circlefit.npy')
                S12 = numpy.load('MR_S12Circlefit.npy')
                S21 = numpy.load('MR_S21Circlefit.npy')
                S22 = numpy.load('MR_S22Circlefit.npy')
                
                ReS11 = S11[freq][0][0]
                ImS11 = S11[freq][0][1]
                ReS12 = S12[freq][0][0]
                ImS12 = S12[freq][0][1]
                ReS21 = S21[freq][0][0]
                ImS21 = S21[freq][0][1]
                ReS22 = S22[freq][0][0]
                ImS22 = S22[freq][0][1]
                
                S_mMag = matlab.double([[ReS11,ReS12], [ReS21,ReS22]]) 
                S_mPhi = matlab.double([[ImS11,ImS12], [ImS21,ImS22]])
                eng.workspace['Me'] = eng.complex(S_mMag, S_mPhi)

    S11 = []
    S12 = []
    S21 = []
    S22 = [] 

    with open('ThruCSV\OutPutMagPhiS11.csv', "rb") as f:
        reader = csv.reader(f, delimiter=",")
        for freq, line in enumerate(reader):
            S11.append(line)
            sweep = freq

    with open('ThruCSV\OutPutMagPhiS12.csv', "rb") as f:
        reader = csv.reader(f, delimiter=",")
        for freq, line in enumerate(reader):
            S12.append(line)

    with open('ThruCSV\OutPutMagPhiS21.csv', "rb") as f:
        reader = csv.reader(f, delimiter=",")
        for freq, line in enumerate(reader):
            S21.append(line)

    with open('ThruCSV\OutPutMagPhiS22.csv', "rb") as f:
        reader = csv.reader(f, delimiter=",")
        for freq, line in enumerate(reader):
            S22.append(line)

    for freq in xrange(sweep): 

        ReS11 = float(S11[freq][0])
        ImS11 = float(S11[freq][1])
        ReS12 = float(S12[freq][0])
        ImS12 = float(S12[freq][1])
        ReS21 = float(S21[freq][0])
        ImS21 = float(S21[freq][1])
        ReS22 = float(S22[freq][0])
        ImS22 = float(S22[freq][1])

        S_mMag = matlab.double([[ReS11, ReS12], [ReS21, ReS22]]) 
        S_mPhi = matlab.double([[ImS11,ImS12], [ImS21,ImS22]])  
        eng.workspace['Ma'] = eng.complex(S_mMag, S_mPhi)

    S11 = []
    S12 = []
    S21 = []
    S22 = [] 

    with open('ReflectCSV\OutPutMagPhiS11.csv', "rb") as f:
        reader = csv.reader(f, delimiter=",")
        for freq, line in enumerate(reader):
            S11.append(line)
            sweep = freq

    with open('ReflectCSV\OutPutMagPhiS12.csv', "rb") as f:
        reader = csv.reader(f, delimiter=",")
        for freq, line in enumerate(reader):
            S12.append(line)

    with open('ReflectCSV\OutPutMagPhiS21.csv', "rb") as f:
        reader = csv.reader(f, delimiter=",")
        for freq, line in enumerate(reader):
            S21.append(line)

    with open('ReflectCSV\OutPutMagPhiS22.csv', "rb") as f:
        reader = csv.reader(f, delimiter=",")
        for freq, line in enumerate(reader):
            S22.append(line)

    for freq in xrange(sweep): 

        ReS11 = float(S11[freq][0])
        ImS11 = float(S11[freq][1])
        ReS12 = float(S12[freq][0])
        ImS12 = float(S12[freq][1])
        ReS21 = float(S21[freq][0])
        ImS21 = float(S21[freq][1])
        ReS22 = float(S22[freq][0])
        ImS22 = float(S22[freq][1])

        S_mMag = matlab.double([[ReS11, ReS12], [ReS21, ReS22]]) 
        S_mPhi = matlab.double([[ImS11,ImS12], [ImS21,ImS22]])  
        eng.workspace['Mc'] = eng.complex(S_mMag, S_mPhi)

    print('\n Ma:\n')
    print(eng.workspace['Ma'])
    print('\n Mb:\n')
    print(eng.workspace['Mb'])
    print('\n Mc:\n')
    print(eng.workspace['Mc'])
    print('\n Md:\n')
    print(eng.workspace['Md'])
    print('\n Me:\n')
    print(eng.workspace['Me'])
    print('\n')


    t12 = 1
    T = 1
    gamma = 1 # equals reflection cooeficient of reflect standard, -1 or 1? 

    eng.workspace['t12'] = eng.complex(t12, 0)
    eng.workspace['T'] = eng.complex(T, 0)
    eng.workspace['gamma'] = eng.complex(gamma,0)

    eng.workspace['N'] = eng.eval('inv(Me - Ma)*(Mb - Me)')
    eng.workspace['O'] = eng.eval('Mb - Mc')
    eng.workspace['R'] = eng.eval('inv(Md - Ma)*(Mb - Md)')
    eng.workspace['P'] = eng.eval('(Ma - Mc)*R')
    eng.workspace['M'] = eng.eval('(Ma - Mc)*N')

    eng.workspace['m'] = eng.eval('(P(2,1) + O(2,1))*M(2,2) - (P(2,2) + O(2,2))*M(2,1)')
    eng.workspace['n'] = eng.eval('O(2,1)*P(1,2) - O(2,2)*P(1,1)')
    eng.workspace['o'] = eng.eval('(M(1,2) + O(1,2))*P(1,1) - (M(1,1) + O(1,1))*P(1,2)')
    eng.workspace['p'] = eng.eval('O(1,2)*M(2,1) - O(1,1)*M(2,2)')

    eng.workspace['t15'] = eng.eval('(-p/o)*(P(1,1)/M(2,2))*(gamma/T)*t12')
    eng.workspace['t14'] = eng.eval('(-M(2,1)/M(2,2))*t12')
    eng.workspace['t13'] = eng.eval('-P(1,2)/P(1,1)*t15')

    eng.workspace['t11'] = eng.eval('(N(2,1)*t13 + N(2,2)*t15)*(1/gamma) - t14*(1/T)')
    eng.workspace['t10'] = eng.eval('(R(2,1)*t12 + R(2,2)*t14)*(1/gamma) - t15*(1/T)')
    eng.workspace['t9'] = eng.eval('(N(1,1)*t13 + N(1,2)*t15)*(1/gamma) - t12*(1/T)')
    eng.workspace['t8'] = eng.eval('(R(1,1)*t12 + R(1,2)*t14)*(1/gamma) - t13*(1/T)')

    eng.workspace['t7'] = eng.eval('Mb(2,1)*t13 + Mb(2,2)*t15')
    eng.workspace['t6'] = eng.eval('Mb(2,1)*t12 + Mb(2,2)*t14')
    eng.workspace['t5'] = eng.eval('Mb(1,1)*t13 + Mb(1,2)*t15')
    eng.workspace['t4'] = eng.eval('Mb(1,1)*t12 + Mb(1,2)*t14')

    eng.workspace['t3'] = eng.eval('Mc(2,1)*t9 + Mc(2,2)*t11 - (1/gamma)*(O(2,1)*t13 + O(2,2)*t15)')
    eng.workspace['t2'] = eng.eval('Mc(2,1)*t8 + Mc(2,2)*t10 - (1/gamma)*(O(1,1)*t12 + O(1,2)*t14)')
    eng.workspace['t1'] = eng.eval('Mc(1,1)*t9 + Mc(1,2)*t11 - (1/gamma)*(O(1,1)*t13 + O(1,2)*t15)')
    eng.workspace['t0'] = eng.eval('Mc(1,1)*t8 + Mc(1,2)*t11 - (1/gamma)*(O(1,1)*t12 + O(1,2)*t14)')

    t0 = eng.workspace['t0']
    t1 = eng.workspace['t1']
    t2 = eng.workspace['t2']
    t3 = eng.workspace['t3']
    t4 = eng.workspace['t4']
    t5 = eng.workspace['t5']
    t6 = eng.workspace['t6']
    t7 = eng.workspace['t7']
    t8 = eng.workspace['t8']
    t9 = eng.workspace['t9']
    t10 = eng.workspace['t10']
    t11 = eng.workspace['t11']
    t12 = eng.workspace['t12']
    t13 = eng.workspace['t13']
    t14 = eng.workspace['t14']
    t15 = eng.workspace['t15']

    Tmatrix = [[t0,t1,t4,t5],[t2,t3,t6,t7],[t8,t9,t12,t13],[t10,t11,t14,t15]]
    matrixprint(Tmatrix)
    csv_out_file_name = 'tmatrix.csv'
    out_file_name = 'tmatrix'
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(Tmatrix))
        line = []
        for row in Tmatrix:
            for val in row:
                line.append(val)
                line = ("{0.real:.3}+{0.imag:.3}j".format(i) for i in line) 
                line = ("{0:>10}".format(i) for i in line)
                line = str(list(line))
                print(line.strip()) 
                spamwriter.writerow(line)
                line = []
    print('\n')
    eng.workspace['T1'] = eng.eval('[t0 t1; t2 t3]')
    eng.workspace['T2'] = eng.eval('[t4 t5; t6 t7]')
    eng.workspace['T3'] = eng.eval('[t8 t9; t10 t11]')
    eng.workspace['T4'] = eng.eval('[t12 t13; t14 t15]')
    eng.workspace['E'] = eng.eval('[T2*T4^(-1) T1-T2*T4^(-1)*T3; T4^(-1) -T4^(-1)*T3]')
    Ematrix = eng.workspace['E']
    matrixprint(Ematrix)
            
    # print('\n Checking Cal Algorithm \n')

    # """
    # To check the cal algorithm we need to correct the original 
    # reflect measurement data with the resulting E matrix, 
    # if the corrected data becomes 'ideal' then mathimatically the cal has worked 

    # Sa = {E3(Sm - E1)^(-1)E2 + E4}^{-1}

    # """
    # Sa_array = []
    # Sm_array = []
    # S11 = []
    # S12 = []
    # S21 = []
    # S22 = [] 

    # with open('ReflectCSV\OutPutMagPhiS11.csv', "rb") as f:
    #     reader = csv.reader(f, delimiter=",")
    #     for freq, line in enumerate(reader):
    #         S11.append(line)
    #         sweep = freq

    # with open('ReflectCSV\OutPutMagPhiS12.csv', "rb") as f:
    #     reader = csv.reader(f, delimiter=",")
    #     for freq, line in enumerate(reader):
    #         S12.append(line)

    # with open('ReflectCSV\OutPutMagPhiS21.csv', "rb") as f:
    #     reader = csv.reader(f, delimiter=",")
    #     for freq, line in enumerate(reader):
    #         S21.append(line)

    # with open('ReflectCSV\OutPutMagPhiS22.csv', "rb") as f:
    #     reader = csv.reader(f, delimiter=",")
    #     for freq, line in enumerate(reader):
    #         S22.append(line)

    # for freq in xrange(sweep): 

    #     ReS11 = float(S11[freq][0])
    #     ImS11 = float(S11[freq][1])
    #     ReS12 = float(S12[freq][0])
    #     ImS12 = float(S12[freq][1])
    #     ReS21 = float(S21[freq][0])
    #     ImS21 = float(S21[freq][1])
    #     ReS22 = float(S22[freq][0])
    #     ImS22 = float(S22[freq][1])

    #     S_mMag = matlab.double([[ReS11, ReS12], [ReS21, ReS22]]) 
    #     S_mPhi = matlab.double([[ImS11,ImS12], [ImS21,ImS22]])  
    #     eng.workspace['Mc'] = eng.complex(S_mMag, S_mPhi)
    #     eng.workspace['E1'] = eng.eval('[E(1,1) E(1,2); E(2,1) E(2,2)]')
    #     eng.workspace['E2'] = eng.eval('[E(1,3) E(1,4); E(2,3) E(2,4)]')
    #     eng.workspace['E3'] = eng.eval('[E(3,1) E(3,2); E(4,1) E(4,2)]')
    #     eng.workspace['E4'] = eng.eval('[E(3,3) E(3,4); E(4,3) E(4,4)]')
    #     eng.workspace['Sa'] = eng.eval('(E3*(Mc - E1)^(-1)*E2 + E4)^(-1)')
        
    #     Sm_22_mag = eng.eval('real(Mc(2,2))')
    #     Sm_22_phi = eng.eval('imag(Mc(2,2))')    
    #     Sa_22_mag = eng.eval('real(Sa(2,2))')
    #     Sa_22_phi = eng.eval('imag(Sa(2,2))')

    #     Sm_11_mag = eng.eval('real(Mc(1,1))')
    #     Sm_11_phi = eng.eval('imag(Mc(1,1))')    
    #     Sa_11_mag = eng.eval('real(Sa(1,1))')
    #     Sa_11_phi = eng.eval('imag(Sa(1,1))')
        
    #     Sm_12_mag = eng.eval('real(Mc(1,2))')
    #     Sm_12_phi = eng.eval('imag(Mc(1,2))')    
    #     Sa_12_mag = eng.eval('real(Sa(1,2))')
    #     Sa_12_phi = eng.eval('imag(Sa(1,2))')
        
    #     Sm_21_mag = eng.eval('real(Mc(2,1))')
    #     Sm_21_phi = eng.eval('imag(Mc(2,1))')    
    #     Sa_21_mag = eng.eval('real(Sa(2,1))')
    #     Sa_21_phi = eng.eval('imag(Sa(2,1))')
        



    #     # matrixprint(Sa)
    #     # Sa = str(Sa)
    #     # Sa = Sa.split(',')

    #     Sa = [Sa_11_mag, Sa_11_phi, Sa_12_mag, Sa_12_phi, Sa_21_mag, Sa_21_phi, Sa_22_mag, Sa_22_phi]
    #     Sm = [Sm_11_mag, Sm_11_phi, Sm_12_mag, Sm_12_phi, Sm_21_mag, Sm_21_phi, Sm_22_mag, Sm_22_phi]
    #     Sa_array.append(Sa)
    #     Sm_array.append(Sm)

        

    # print(Sa_array)
    # csv_out_file_name = 'Sa.csv'
    # out_file_name = 'Sa'
    # with open(csv_out_file_name, 'w') as csvfile:
    #     spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
    #     numpy.save(out_file_name, numpy.array(Sa_array))
    #     for row in Sa_array:
    #         print(row)
    #         spamwriter.writerow(row)

    # print(Sm_array)
    # csv_out_file_name = 'Sm.csv'
    # out_file_name = 'Sm'
    # with open(csv_out_file_name, 'w') as csvfile:
    #     spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
    #     numpy.save(out_file_name, numpy.array(Sm_array))
    #     for row in Sm_array:
    #         print(row)
    #         spamwriter.writerow(row)


    print('\n Checking Cal Algorithm \n')

    """
    To check the cal algorithm we need to correct the original 
    reflect measurement data with the resulting E matrix, 
    if the corrected data becomes 'ideal' then mathimatically the cal has worked 

    Sa = {E3(Sm - E1)^(-1)E2 + E4}^{-1}

    """
    Sa_array = []
    Sm_array = []
    S11 = []
    S12 = []
    S21 = []
    S22 = [] 

    with open('ThruCSV\OutPutMagPhiS11.csv', "rb") as f:
        reader = csv.reader(f, delimiter=",")
        for freq, line in enumerate(reader):
            S11.append(line)
            sweep = freq

    with open('ThruCSV\OutPutMagPhiS12.csv', "rb") as f:
        reader = csv.reader(f, delimiter=",")
        for freq, line in enumerate(reader):
            S12.append(line)

    with open('ThruCSV\OutPutMagPhiS21.csv', "rb") as f:
        reader = csv.reader(f, delimiter=",")
        for freq, line in enumerate(reader):
            S21.append(line)

    with open('ThruCSV\OutPutMagPhiS22.csv', "rb") as f:
        reader = csv.reader(f, delimiter=",")
        for freq, line in enumerate(reader):
            S22.append(line)

    for freq in xrange(sweep): 

        ReS11 = float(S11[freq][0])
        ImS11 = float(S11[freq][1])
        ReS12 = float(S12[freq][0])
        ImS12 = float(S12[freq][1])
        ReS21 = float(S21[freq][0])
        ImS21 = float(S21[freq][1])
        ReS22 = float(S22[freq][0])
        ImS22 = float(S22[freq][1])

        S_mMag = matlab.double([[ReS11, ReS12], [ReS21, ReS22]]) 
        S_mPhi = matlab.double([[ImS11,ImS12], [ImS21,ImS22]])  
        eng.workspace['Mc'] = eng.complex(S_mMag, S_mPhi)
        eng.workspace['E1'] = eng.eval('[E(1,1) E(1,2); E(2,1) E(2,2)]')
        eng.workspace['E2'] = eng.eval('[E(1,3) E(1,4); E(2,3) E(2,4)]')
        eng.workspace['E3'] = eng.eval('[E(3,1) E(3,2); E(4,1) E(4,2)]')
        eng.workspace['E4'] = eng.eval('[E(3,3) E(3,4); E(4,3) E(4,4)]')
        eng.workspace['Sa'] = eng.eval('(E3*(Mc - E1)^(-1)*E2 + E4)^(-1)')
        
        Sm_22_mag = eng.eval('real(Mc(2,2))')
        Sm_22_phi = eng.eval('imag(Mc(2,2))')    
        Sa_22_mag = eng.eval('real(Sa(2,2))')
        Sa_22_phi = eng.eval('imag(Sa(2,2))')

        Sm_11_mag = eng.eval('real(Mc(1,1))')
        Sm_11_phi = eng.eval('imag(Mc(1,1))')    
        Sa_11_mag = eng.eval('real(Sa(1,1))')
        Sa_11_phi = eng.eval('imag(Sa(1,1))')
        
        Sm_12_mag = eng.eval('real(Mc(1,2))')
        Sm_12_phi = eng.eval('imag(Mc(1,2))')    
        Sa_12_mag = eng.eval('real(Sa(1,2))')
        Sa_12_phi = eng.eval('imag(Sa(1,2))')
        
        Sm_21_mag = eng.eval('real(Mc(2,1))')
        Sm_21_phi = eng.eval('imag(Mc(2,1))')    
        Sa_21_mag = eng.eval('real(Sa(2,1))')
        Sa_21_phi = eng.eval('imag(Sa(2,1))')
        



        # matrixprint(Sa)
        # Sa = str(Sa)
        # Sa = Sa.split(',')

        Sa = [Sa_11_mag, Sa_11_phi, Sa_12_mag, Sa_12_phi, Sa_21_mag, Sa_21_phi, Sa_22_mag, Sa_22_phi]
        Sm = [Sm_11_mag, Sm_11_phi, Sm_12_mag, Sm_12_phi, Sm_21_mag, Sm_21_phi, Sm_22_mag, Sm_22_phi]
        Sa_array.append(Sa)
        Sm_array.append(Sm)

        

    print(Sa_array)
    csv_out_file_name = 'Sa.csv'
    out_file_name = 'Sa'
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(Sa_array))
        for row in Sa_array:
            print(row)
            spamwriter.writerow(row)

    print(Sm_array)
    csv_out_file_name = 'Sm.csv'
    out_file_name = 'Sm'
    with open(csv_out_file_name, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', lineterminator = '\n')            
                    
        numpy.save(out_file_name, numpy.array(Sm_array))
        for row in Sm_array:
            print(row)
            spamwriter.writerow(row)

    # data = numpy.loadtxt("Sm.csv", delimiter=",")#[::100]
    # print(data)

    # VecRe = data[:, 0].tolist() 
    # VecIm = data[:, 1].tolist()

    # print(VecRe)

    # for i in xrange(1, len(VecRe)):
    #     MagSm = 20*math.log(math.sqrt(math.pow(VecRe[i],2)+math.pow(VecIm[i],2)))
    #     PhiSm = math.atan2(VecIm[i],VecRe[i])
    #     print(MagSm)

    # data = numpy.loadtxt("Sa.csv", delimiter=",")#[::100]

    # VecRe = data[:, 0].tolist()
    # VecIm = data[:, 1].tolist()

    # for i in xrange(1, len(VecRe)):
    #     MagSa = 20*math.log(math.sqrt(math.pow(VecRe[i],2)+math.pow(VecIm[i],2)))
    #     PhiSa = math.atan2(VecIm[i],VecRe[i])

    #     print(MagSa)

    #     # Phase = arctan(Imaginary/Real)

    # # val1 = MagSa + PhiSa*1j

    # # pp.figure(figsize=(8, 8))

    # # ax = pp.subplot(1, 1, 1, projection='smith')
    # # #pp.plot([10, 100], markevery=1)

    # # pp.plot(50*val1, label="Captured Data", datatype=SmithAxes.Z_PARAMETER)

    # # pp.legend(loc="lower right", fontsize=12)
    # # pp.title("HP3495A Capture Trace - Smith Chart")

    # # pp.savefig("SmithChartExport.pdf", format="pdf", bbox_inches="tight")
    # # pp.show()
    # # end     

    # balloon_tip("AVNA Job", "Calibration Complete")

# except:
#     balloon_tip("AVNA Job", "Calibration Failed!")
#     ctypes.windll.user32.MessageBoxW(0, u"Error", u"Error", 0)

""" spare spare code"""
    # Imrows = matlab.double([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]])
    # eng.workspace['Tmatrix'] = Re
    # matrixprint(eng.workspace['Tmatrix'])
    # print(eng.workspace['t0'])

    # eng.eval('Tmatrix(1,1) = t0')
    # eng.eval('Tmatrix(1,2) = t1')
    # eng.eval('Tmatrix(1,3) = t2')
    # eng.eval('Tmatrix(1,4) = t3')
    # eng.eval('Tmatrix(2,1) = t4')
    # eng.eval('Tmatrix(2,2) = t5')
    # eng.eval('Tmatrix(2,3) = t6')
    # eng.eval('Tmatrix(2,4) = t7')
    # eng.eval('Tmatrix(3,1) = t8')
    # eng.eval('Tmatrix(3,2) = t9')
    # eng.eval('Tmatrix(3,3) = t10')
    # eng.eval('Tmatrix(3,4) = t11')
    # eng.eval('Tmatrix(4,1) = t12')
    # eng.eval('Tmatrix(4,2) = t13')
    # eng.eval('Tmatrix(4,3) = t14')
    # eng.eval('Tmatrix(4,4) = t15')
    # eng.workspace['Tmatrix(1,2)'] = eng.workspace['t1']
    # eng.workspace['Tmatrix(1,3)'] = eng.workspace['t2']
    # eng.workspace['Tmatrix(1,4)'] = eng.workspace['t3']
    # eng.workspace['Tmatrix(2,1)'] = eng.workspace['t4']
    # eng.workspace['Tmatrix(2,2)'] = eng.workspace['t5']
    # eng.workspace['Tmatrix(2,3)'] = eng.workspace['t6']
    # eng.workspace['Tmatrix(2,4)'] = eng.workspace['t7']
    # eng.workspace['Tmatrix(3,1)'] = eng.workspace['t8']
    # eng.workspace['Tmatrix(3,2)'] = eng.workspace['t9']
    # eng.workspace['Tmatrix(3,3)'] = eng.workspace['t10']
    # eng.workspace['Tmatrix(3,4)'] = eng.workspace['t11']
    # eng.workspace['Tmatrix(4,1)'] = eng.workspace['t12']
    # eng.workspace['Tmatrix(4,2)'] = eng.workspace['t13']
    # eng.workspace['Tmatrix(4,3)'] = eng.workspace['t14']
    # eng.workspace['Tmatrix(4,4)'] = eng.workspace['t15']


    # Tmatrix = eng.workspace['Tmatrix']
    # matrixprint(Tmatrix)

    # check = eng.eval('t15/t12')
    # cal_check = input('Is {0} close to 1?, y/n: ')

""" Spare code? 
    # S_mMag = matlab.double([[1,2], [5,6]]) 
    # S_mPhi = matlab.double([[3,4], [7,8]])

    # S_aMag = matlab.double([[1,2], [5,6]])
    # S_aPhi = matlab.double([[3,4], [7,8]])

    # real = matlab.double([[1,2], [3,4]])
    # img = matlab.double([[5,6], [7,8]])

    # eng.workspace['Sm'] = eng.complex(S_mMag, S_mPhi)
    # eng.workspace['Sa'] = eng.complex(S_aMag, S_aPhi)

    # eng.workspace['T1'] = eng.complex(real, img)
    # eng.workspace['T2'] = eng.complex(real, img)
    # eng.workspace['T3'] = eng.complex(real, img)
    # eng.workspace['T4'] = eng.complex(real, img)

    # Sa = eng.workspace['Sa'] 
    # Sm = eng.workspace['Sm']


    # Equations to be solved: (latex format) 

    # \[S_m = (T_1S_a + T_2)(T_3S_a + T_4)^{-1}\]
    # \[T_1S_a + T_2 -S_mT_3S_a -S_mT_4 = 0\]
    # \[S_a = (T_1 - S_mT_3)^{-1}(S_mT_4 -T_2)\]
    

    # print('S parameters actual: ')
    # matrixprint(Sa)
    # print('S parameters Measured: ')
    # matrixprint(Sm)

    # # get matrix 'A' from these equations (make it up in the short term with 'magic')
    # eng.workspace['A'] = eng.magic(16)
    # A = eng.workspace['A']

    # #matrices are solved with SVD method. can do this directly with matlab:
    # eng.workspace['USV'] = eng.svd(A,nargout=3)
    # USV = eng.workspace['USV']
    # U = USV[0]
    # S = USV[1]
    # V = USV[2]

    # eng.workspace['U'] = USV[0]
    # eng.workspace['S'] = USV[1]
    # eng.workspace['V'] = USV[2]
    # line = []

    # print('A is:')
    # matrixprint(A)

    # print('SVD result for A is: ')
    # print('U: ')
    # matrixprint(U)

    # print('S:')
    # matrixprint(S)

    # print('V:')
    # matrixprint(V)

    # eng.workspace['B'] = eng.rand(16,1)
    # B = eng.workspace['B']
    # # With U, S, V; T = V.S.U^t.B

    # T = eng.eval('V*inv(S)*transpose(U)*B')

    # print('The T parameters are: ')
    # matrixprint(T)



    # session = matlab.engine.find_matlab
    # print(session)
    # connection = matlab.engine.connect_matlab
    # print(connection)

    # M = eng.magic(4)
    # for x in M: 
    #     print(x)

    # try:
    #     Calibrate()
    # except:
    #     print "Unexpected error:", sys.exc_info()[0]
              
    # #smith plot    
    # data = numpy.loadtxt("OutPutCSV\OutPutMagPhi1.csv", delimiter=",")
    # val1 = data[:, 0] *0.1 + data[:, 1]* 0.24j #Multipliers in this line approximate internal 1st order calibration done inside the 4395A 
     
    # pp.figure(figsize=(8, 8))

    # ax = pp.subplot(1, 1, 1, projection='smith')

    # pp.plot(50*val1, label="Captured Data", datatype=SmithAxes.Z_PARAMETER)

    # pp.legend(loc="lower right", fontsize=12)
    # pp.title("HP3495A Capture Trace - Smith Chart")

    # pp.savefig("SmithChartExport.pdf", format="pdf", bbox_inches="tight")
    # pp.show() """
